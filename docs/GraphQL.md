### O que é o GraphQL  
  
Uma API em GraphQL [não deixa de ser uma Rest API](https://medium.com/codingthesmartway-com-blog/rest-vs-graphql-418eac2e3083), a maior diferença é que ao invés de você   utilizar parâmetros na *search query* da URL para obter resultados específicos, você irá utilizar uma *query language* própria do padrão de comunicação do GraphQL, cujos benefícios incluem:  
  
* Esquema e estrutura de dados organizada que pode ser utilizado pela aplicação cliente para validação ou mesmo para gerar definições de tipo para a linguagem do cliente
* Solução do problema de *Over Fetching* e *Under Fetching*, ou seja, você obtém da API exatamente os campos de dados que você precisa
* Todos os campos de dados retornados podem ser renomeados no momento da requisição para manter alguma lógica de negócio do cliente que está utilizando a API
  
#### Como ele se parece  
```  
POST graph.4all.com/marketplace  
Header Content-Type application/json  

{
   "query": "{ me { userInfo { id4All, fullName, emailAddress } } }"  
}  
```  
  
##### Resposta
  
```  
{  
  "data": {  
    "me": {  
      "userInfo": {  
        "id4All": "5",  
        "type": "USER",  
        "fullName": "John Doe",  
        "email": "example@example.com"  
      }  
    }  
  }  
}  
```  

Para entender mais a fundo como funciona a montagem das *queries* você pode consultar a [documentação oficial](https://graphql.org/learn/) . 