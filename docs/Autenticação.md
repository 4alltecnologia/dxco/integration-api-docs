
# Autenticação  
  
A autenticação é baseada em token de acesso que deve ser passado no cabeçalho de cada requisição HTTP feita na API,

## Como obter

| Método  | Autenticação | Rota  | Formato de Resposta | 
|------|------|-------|-------|
| `GET`  |  Sim  | `/token` |  application/json  |  

#### Parâmetros de Cabeçalho

Nenhum parâmetro

#### Parâmetros de Rota

Nenhum parâmetro

#### Parâmetros de Query

Nenhum parâmetro

#### Parâmetros de body

| Parâmetro  | Tipo | Opcional | Descrição |
|------|------|------|------|
| `clientId`  |  [String](public/schema/string.doc.html) | Não | Id do cliente
| `clientSecret`  |  [String](public/schema/string.doc.html) | Não  | Segredo do cliente
| `marketplaceId`  |  [ID](public/schema/id.doc.html) | Sim  | Id do marketplace

#### Amostras

##### Amostra #1

###### Curl Request
```
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X POST http://%API_URL%/token -d '{ "clientId": "myid", "clientSecret": "mysecret" }'
```
###### Response
```
{
  "success": true,
  "data": {
    "token": "your-encrypted-token"
  }
}
```

## Como usar

Uma vez obtido o token de acesso você pode agora passá-lo em todas as suas requisições da api através do parâmetro de cabeçalho `X-Authentication`.

#### Amostras

##### Amostra #1

###### Curl Request
```
curl -i -H "Accept: application/json" -H "X-Authentication: your-encrypted- token" -H "Content-Type: application/json" -X POST http://%API_URL%/ -d '{ "query": "{ me { userInfo { id4All, fullName } } }" }'
```
###### Response
```
{
  "success": true,
  "data": {
    "me": {
	  "userInfo": {
	    "id4All": "5",
		"fullName": "John Doe"
	  }
    }
  }
}
```